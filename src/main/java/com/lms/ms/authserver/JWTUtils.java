package com.lms.ms.authserver;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.security.core.userdetails.UserDetails;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JWTUtils {

	private static String SECRETE_KEY = "1234";

	public Claims extractAllClaims(String token) {
		Claims claims = Jwts.parser().setSigningKey(SECRETE_KEY).parseClaimsJws(token).getBody();
		return claims;
	}

	public String extractSubject(String token) {
		return extractClaim(token, Claims::getSubject);
	}

	public Date extractExpiration(String token) {
		return extractClaim(token, Claims::getExpiration);
	}

	public <T> T extractClaim(String token, Function<Claims, T> claimExtractor) {
		Claims claims = extractAllClaims(token);
		return claimExtractor.apply(claims);

	}

	public boolean isTokenExpired(String token) {
		return extractExpiration(token).before(new Date());
	}

	public static String generateToken(UserDetails details) {
		return createToken(new HashMap<String, Object>(), details.getUsername());
	}

	private static String createToken(Map<String, Object> claims, String subject) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, 10);
		cal.getTime();
		return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(cal.getTime()).signWith(SignatureAlgorithm.HS256, SECRETE_KEY).compact();
	}
	
	public boolean validateToken() {
		return true;
	}
}
