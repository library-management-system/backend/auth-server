package com.lms.ms.authserver.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lms.ms.authserver.JWTUtils;
import com.lms.ms.authserver.model.AuthenticationRequestModel;
import com.lms.ms.authserver.model.AuthenticationResponse;
import com.lms.ms.authserver.service.MyUserDetailsService;

@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	MyUserDetailsService myUserDetailsService;

	@Autowired
	AuthenticationManager authenticationManager;

	@PostMapping("/authenticate")
	public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequestModel requestModel)
			throws Exception {
		try {
			Authentication authenticate = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(requestModel.getUserName(), requestModel.getPassword()));
			System.out.println(authenticate);
		} catch (BadCredentialsException exception) {
			// TODO : Throw exception
			throw new Exception("incorrect username or password");
		}

		final UserDetails userDetails = myUserDetailsService.loadUserByUsername(requestModel.getUserName());

		final String token = JWTUtils.generateToken(userDetails);
		return ResponseEntity.ok(new AuthenticationResponse(token));
	}
}
